# Ujicoba vega + kroki

- pastikan java sudah terinstall
- buka terminal, lalu jalankan script `start.sh` untuk memulai server
- gunakan curl/httpie/yang lain untuk generate chart. Contoh menggunakan `test.vg.json` dan httpie:

    $ cat test.vg.json | http POST localhost:8000/vegalite/png Content-Type:text/plain > result.png